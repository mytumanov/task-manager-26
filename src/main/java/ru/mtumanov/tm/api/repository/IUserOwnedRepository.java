package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractEntityNotFoundException;

    @NotNull
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    M remove(@NotNull String userId, @NotNull M model) throws AbstractEntityNotFoundException;

    @NotNull
    M removeById(@NotNull String userId, @NotNull String id) throws AbstractEntityNotFoundException;

    @NotNull
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

    void clear(@NotNull String userId);

    int getSize(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

}
